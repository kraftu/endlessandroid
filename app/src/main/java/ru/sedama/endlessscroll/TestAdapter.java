package ru.sedama.endlessscroll;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.awt.font.TextAttribute;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by kraftu on 11.09.15.
 */
public class TestAdapter extends BaseAdapter implements Parcelable {


    public TestAdapter(Context context) {
        this.context = context;
    }

    Context context;
    ArrayList<String> arrayList = new ArrayList<>();

    @Override
    public int getCount() {
        return arrayList.size();
    }
    public void setData( ArrayList<String> arrayList){
        this.arrayList =arrayList;
        notifyDataSetChanged();
    }

    public void addData( ArrayList<String> arrayList){
        this.arrayList.addAll(arrayList);
        notifyDataSetChanged();
    }
    @Override
    public Object getItem(int i) {
        return arrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        TextView tv = new TextView(context);
        tv.setText((String)getItem(i));
        return tv;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.arrayList);
    }

    private TestAdapter(Parcel in) {
        this.arrayList = (ArrayList<String>) in.readSerializable();
    }

    public static final Parcelable.Creator<TestAdapter> CREATOR = new Parcelable.Creator<TestAdapter>() {
        public TestAdapter createFromParcel(Parcel source) {
            return new TestAdapter(source);
        }

        public TestAdapter[] newArray(int size) {
            return new TestAdapter[size];
        }
    };
}
