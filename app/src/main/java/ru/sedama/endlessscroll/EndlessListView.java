package ru.sedama.endlessscroll;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.WrapperListAdapter;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by kraftu on 11.09.15.
 */
public class EndlessListView extends ListView implements AbsListView.OnScrollListener {

    public static final int PAGE_SIZE = 10;
    // The minimum amount of items to have below your current scroll position
    // before loading more.
    private int visibleThreshold = 5;
    // The current offset index of data you have loaded
    private int currentPage = 0;
    // The total number of items in the dataset after the last load
    private int previousTotalItemCount = 0;
    // True if we are still waiting for the last set of data to load.
    private boolean loading = true;
    // Sets the starting page index
    private int startingPageIndex = 0;
    View progressView;
    boolean addedFooter = false;

    public EndlessListView(Context context) {
        super(context);
    }

    public EndlessListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setOnScrollListener(this);
        progressView = new ProgressBar(getContext());
    }

    public EndlessListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.setOnScrollListener(this);
        progressView = new ProgressBar(getContext());
    }


    public void setEndlessScrollListener(int visibleThreshold, int startPage) {
        this.visibleThreshold = visibleThreshold;
        this.startingPageIndex = startPage;
        this.currentPage = startPage;
    }

    // This happens many times a second during a scroll, so be wary of the code you place here.
    // We are given a few useful parameters to help us work out if we need to load some more data,
    // but first we check if we are waiting for the previous load to finish.
    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        // If the total item count is zero and the previous isn't, assume the
        // list is invalidated and should be reset back to initial state
        if (totalItemCount < previousTotalItemCount) {

            this.currentPage = this.startingPageIndex;
            this.previousTotalItemCount = totalItemCount;
            if (totalItemCount == 0) {
                this.loading = true;
            }
        }

        // If it’s still loading, we check to see if the dataset count has
        // changed, if so we conclude it has finished loading and update the current page
        // number and total item count.
        if (loading && (totalItemCount > previousTotalItemCount)) {
            loading = false;
            previousTotalItemCount = totalItemCount;
            currentPage++;
        }

        // If it isn’t currently loading, we check to see if we have breached
        // the visibleThreshold and need to reload more data.
        // If we do need to reload some more data, we execute onLoadMore to fetch the data.
        if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
            onLoadMore(currentPage + 1, totalItemCount);
            loading = true;
        }
    }

    // Defines the process for actually loading more data based on page
    public void onLoadMore(final int page, int totalItemsCount) {
        Toast.makeText(getContext(), String.format("String page:%d totalItemsCount:%d", page, totalItemsCount), Toast.LENGTH_SHORT).show();
        if (getFooterViewsCount() == 0)
            addFooterView(progressView);


        postDelayed(new Runnable() {
            @Override
            public void run() {
                TestAdapter ad;
                if (getAdapter() instanceof WrapperListAdapter) {

                    ad = ((TestAdapter) ((WrapperListAdapter) getAdapter()).getWrappedAdapter());
                } else {

                    ad = (TestAdapter) getAdapter();
                }


                String[] data = new String[PAGE_SIZE];
                for (int i = 0; i < PAGE_SIZE; i++) {
                    data[i] = "Page:" + page + " " + "item:" + i;
                }
                ad.addData(new ArrayList<String>(Arrays.asList(data)));
            }
        }, 4000);

    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        // Don't take any action on changed
    }

    @Override
    public Parcelable onSaveInstanceState() {
        //begin boilerplate code that allows parent classes to save state
        Parcelable superState = super.onSaveInstanceState();

        SaveState ss = new SaveState(superState);
        //end

        ss.visibleThreshold = visibleThreshold;
        ss.currentPage = currentPage;
        ss.previousTotalItemCount = previousTotalItemCount;
        ss.loading = loading;
        ss.startingPageIndex = startingPageIndex;
        return ss;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        //begin boilerplate code so parent classes can restore state
        if (!(state instanceof SaveState)) {
            super.onRestoreInstanceState(state);
            return;
        }

        SaveState ss = (SaveState) state;
        super.onRestoreInstanceState(ss.getSuperState());
        //end

        visibleThreshold = ss.visibleThreshold;
        currentPage = ss.currentPage;
        previousTotalItemCount = ss.previousTotalItemCount;
        loading = ss.loading;
        startingPageIndex = ss.startingPageIndex;
        if (loading) addFooterView(new ProgressBar(getContext()));
    }

    static class SaveState extends BaseSavedState {
        // The minimum amount of items to have below your current scroll position
        // before loading more.
        int visibleThreshold;
        // The current offset index of data you have loaded
        int currentPage;
        // The total number of items in the dataset after the last load
        int previousTotalItemCount;
        // True if we are still waiting for the last set of data to load.
        boolean loading;
        // Sets the starting page index
        int startingPageIndex;

        SaveState(Parcelable superState) {
            super(superState);
        }

        private SaveState(Parcel in) {
            super(in);

            this.visibleThreshold = in.readInt();
            this.currentPage = in.readInt();
            this.previousTotalItemCount = in.readInt();
            this.loading = in.readByte() == 1 ? true : false;
            this.startingPageIndex = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(this.visibleThreshold);
            out.writeInt(this.currentPage);
            out.writeInt(this.previousTotalItemCount);
            out.writeByte((byte) (loading == true ? 1 : 0));
            out.writeInt(this.startingPageIndex);
        }

        //required field that makes Parcelables from a Parcel
        public static final Parcelable.Creator<SaveState> CREATOR =
                new Parcelable.Creator<SaveState>() {
                    public SaveState createFromParcel(Parcel in) {
                        return new SaveState(in);
                    }

                    public SaveState[] newArray(int size) {
                        return new SaveState[size];
                    }
                };
    }

}