package ru.sedama.endlessscroll;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Created by kraftu on 21.09.15.
 */
public class FrameContent extends FrameLayout {

    public final static int STATE_PROGRESS = 1;
    public final static int STATE_SHOW_CONTENT = 2;
    public final static int STATE_REPEAT = 3;

    private int currentState;

    private View targetView;
    private View body;
    private View progress;
    private View repeat;

    RefreshViewListener refreshViewListener;

    private int layoutIdProgress = -1;
    private int layoutIdRepeat = -1;

    public FrameContent(Context context) {
        super(context);
    }

    public FrameContent(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.FrameContent, 0, 0);
        try {
            layoutIdProgress = a.getResourceId(R.styleable.FrameContent_layoutProgressBar, -1);
            layoutIdRepeat = a.getResourceId(R.styleable.FrameContent_layoutRepeat, -1);
        } finally {
            a.recycle();
        }

    }

    public FrameContent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }


    private View getLayoutProgressBar() {
        if (layoutIdProgress == -1) {
            return new ProgressBar(getContext());
        }
        return LayoutInflater.from(getContext()).inflate(layoutIdProgress, null);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        initView();
    }

    private View getLayoutRepeat() {
        if (layoutIdRepeat == -1) {

            LinearLayout root = new LinearLayout(getContext());
            root.setOrientation(LinearLayout.VERTICAL);

            TextView textView = new TextView(getContext());
            textView.setText("Failed to load data, yet again");

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            lp.gravity = Gravity.CENTER;

            root.addView(textView, lp);

            Button button = new Button(getContext());
            button.setText("Repeat");
            root.addView(button, lp);

            return root;
        }

        return LayoutInflater.from(getContext()).inflate(layoutIdRepeat, null);
    }

    private View getViewButton(View view) {
        View viewButton = null;
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View v = viewGroup.getChildAt(i);
                viewButton = getViewButton(v);
                if (viewButton != null) break;
            }
        } else if (view instanceof Button) {
            viewButton = view;
        }
        return viewButton;
    }

    private void initView() {
        FrameLayout.LayoutParams layoutParams = null;
        if (getChildCount() != 1) {
            new RuntimeException("FrameContent must contain one child ");
        }
        body = getChildAt(0);

        repeat = getLayoutRepeat();
        View viewButton = getViewButton(repeat);

        if (viewButton == null) {
            new RuntimeException("FrameContent should contain button LayoutRepeat");
        }

        viewButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (refreshViewListener != null) {
                    if (refreshViewListener.onClickRepeat(FrameContent.this))
                        setState(FrameContent.STATE_PROGRESS);
                }
            }
        });

        layoutParams = (FrameLayout.LayoutParams) new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        layoutParams.gravity = Gravity.CENTER;
        addView(repeat, layoutParams);

        layoutParams = (FrameLayout.LayoutParams) new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.CENTER;

        progress = getLayoutProgressBar();
        addView(progress, layoutParams);

        setState(STATE_PROGRESS);
    }

    public void initViewTarget(View targetView) {
        ViewParent viewParent = targetView.getParent();
        if (viewParent != null && viewParent instanceof ViewGroup) {
            ViewGroup rViewGroup = (ViewGroup) viewParent;
            rViewGroup.removeView(targetView);
            rViewGroup.addView(this, ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            addView(targetView);
            initView();
        } else new RuntimeException("targetView not be null");
    }

    public RefreshViewListener getRefreshViewListener() {
        return refreshViewListener;
    }

    public void setRefreshViewListener(RefreshViewListener refreshViewListener) {
        this.refreshViewListener = refreshViewListener;
    }

    private void setStateProgress(boolean state) {
        if (state)
            progress.setVisibility(View.VISIBLE);
        else
            progress.setVisibility(View.GONE);
    }

    private void setStateRepeat(boolean state) {
        if (state)
            repeat.setVisibility(View.VISIBLE);
        else
            repeat.setVisibility(View.GONE);
    }

    private void setStateContent(boolean state) {
        if (state)
            body.setVisibility(View.VISIBLE);
        else
            body.setVisibility(View.GONE);
    }

    public void setState(final int state) {
        if (currentState == state)
            return;
        currentState = state;
        Log.d(getClass().getName(), String.valueOf(state));
        if (state == STATE_PROGRESS) {
            setStateProgress(true);
            setStateRepeat(false);
            setStateContent(false);
        } else if (state == STATE_REPEAT) {
            setStateProgress(false);
            setStateRepeat(true);
            setStateContent(false);
        } else if (state == STATE_SHOW_CONTENT) {
            setStateProgress(false);
            setStateRepeat(false);
            setStateContent(true);
        }

    }

    public interface RefreshViewListener {
        boolean onClickRepeat(FrameContent refreshView);
    }
}
