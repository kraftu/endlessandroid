package ru.sedama.endlessscroll;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ProgressBar;

/**
 * Created by kraftu on 22.09.15.
 */
public class TdProgressBar extends ProgressBar {
    public TdProgressBar(Context context) {
        super(context);
        init();
    }

    public TdProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TdProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setIndeterminateDrawable(getDrawableAnimation(R.drawable.td_progress_bar));

    }

    private Drawable getDrawableAnimation(int idDrawable) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return getResources().getDrawable(idDrawable, getContext().getTheme());
        } else {
            return getResources().getDrawable(idDrawable);
        }
    }
}
